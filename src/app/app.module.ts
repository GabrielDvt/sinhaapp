import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SQLite, SQLiteObject  } from '@ionic-native/sqlite'
import { DatabaseProvider } from '../providers/database/database';
import { HttpClientModule } from '@angular/common/http';
import { ClientesProvider } from '../providers/clientes/clientes';
import { FuncionariosProvider } from '../providers/funcionarios/funcionarios';
import { VendasProvider } from '../providers/vendas/vendas';
import { ProdutosProvider } from '../providers/produtos/produtos';

import { HttpModule } from '@angular/http';
import { LojasProvider } from '../providers/lojas/lojas';
import { UnidadesProvider } from '../providers/unidades/unidades';
import { EstoquesProvider } from '../providers/estoques/estoques';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider,
    HttpClientModule,
    ClientesProvider,
    FuncionariosProvider,
    VendasProvider,
    ProdutosProvider,
    LojasProvider,
    UnidadesProvider,
    EstoquesProvider,
  ]
})
export class AppModule {}
