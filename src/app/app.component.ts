import { Component, ViewChild, Injectable } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DatabaseProvider } from '../providers/database/database';


@Component({
  templateUrl: 'app.html'
})


export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = "RegistrarVendasPage";

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, dbProvider: DatabaseProvider) {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();

      //Criando o banco de dados
      dbProvider.createDatabase()
        .then(() => {
          // fechando a SplashScreen somente quando o banco for criado
          this.openHomePage(splashScreen);
        })

        .catch(() => {
          // ou se houver erro na criação do banco
          this.openHomePage(splashScreen);
        });

      this.splashScreen.hide();
    });

    // used for an example of ngFor and navigation
    this.pages = [
      // { title: 'Estatísticas', component: "EstatisticasPage", icon: "md-stats"},
      { title: 'Registrar vendas', component: "RegistrarVendasPage", icon: "md-clipboard"},
      { title: 'Listar vendas', component: "VendasPage", icon: "md-book"},
      { title: 'Estoque', component: "EstoquePage", icon: 'logo-dropbox'},
      { title: 'Produtos', component: "ProdutosPage", icon: "md-cube"},
      { title: 'Funcionários', component: "FuncionariosPage", icon: "md-person"},
      { title: 'Clientes', component: "ClientesPage", icon: "md-people" }
    ];
  }

  private openHomePage(splashScreen: SplashScreen) {
    splashScreen.hide();
    this.rootPage = "ProdutosPage";
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
