import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstoquePorLojaPage } from './estoque-por-loja';

@NgModule({
  declarations: [
    EstoquePorLojaPage,
  ],
  imports: [
    IonicPageModule.forChild(EstoquePorLojaPage),
  ],
})
export class EstoquePorLojaPageModule {}
