import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Loja, LojasProvider } from '../../providers/lojas/lojas';
import { AddLojaPage } from '../add-loja/add-loja';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the LojasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lojas',
  templateUrl: 'estoque-por-loja.html',
})
export class EstoquePorLojaPage {

  lojas: any;

  constructor(public navCtrl: NavController, public http: HttpClient, public loading: LoadingController, public navParams: NavParams,  private alertCtrl: AlertController, private sqlite: SQLite, private toast: ToastController, private lojaProvider: LojasProvider) {
        
  }

  ionViewDidEnter() {
    let loader = this.loading.create({content: 'Buscando lojas...'});
    loader.present().then(() => {
      this.getLojas();
    });
    loader.dismiss();
  }

  getLojas() {
    this.lojaProvider.getAll()
      .then(data => {
        this.lojas = data;
      });  
  }

  selecionarLoja(loja: Loja) {
    this.navCtrl.push("EstoquePorLojaSelecionadaPage", {loja: loja});
  }

  adicionar() {
    this.navCtrl.push('AddLojaPage');
  }

  editarLoja(id: number) {
    this.navCtrl.push('AddLojaPage', {id: id});
  }

  removerLoja(loja: Loja) {
     let alert = this.alertCtrl.create({
       title: 'Confirmar ação',
       message: 'Deseja realmente excluir este loja?',
       buttons: [
         {
           text: 'Cancelar',
           role: 'cancelar',
           handler: () => {
           }
         },
         {
           text: 'Confirmar',
           handler: () => {
             this.lojaProvider.remove(loja.id)
               .then(() => {
                 // Removendo do array de produtos
                 var index = this.lojas.indexOf(loja);
                 this.lojas.splice(index, 1);
                 this.toast.create({ message: 'Loja removida.', duration: 3000, position: 'botton' }).present();
               });
           }
         }
       ]
     });
     alert.present();
  }
   
}
