import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddFuncionarioPage } from './add-funcionario';

@NgModule({
  declarations: [
    AddFuncionarioPage,
  ],
  imports: [
    IonicPageModule.forChild(AddFuncionarioPage),
  ],
})
export class AddFuncionarioPageModule {}
