import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController, NavParams, ToastController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Funcionario, FuncionariosProvider } from '../../providers/funcionarios/funcionarios';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the AddFuncionarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-funcionario',
  templateUrl: 'add-funcionario.html',
})

export class AddFuncionarioPage {

  funcionario: Funcionario;
  formValidation: FormGroup;
  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController, formBuilder: FormBuilder, public navParams: NavParams, public loading: LoadingController, private toast: ToastController, private sqlite: SQLite, private funcionarioProvider: FuncionariosProvider) {
    this.funcionario = new Funcionario();
    
      if (this.navParams.data.id) {
        let loader = this.loading.create({content: 'Carregando dados...'});
        loader.present();
        this.funcionarioProvider.get(this.navParams.data.id)
          .then((result: any) => {
            this.funcionario = result;
          })
        loader.dismiss();  
      }

      this.formValidation = formBuilder.group({
        nome: ['', [Validators.maxLength(200), Validators.required]]
      });

  }

  ionViewDidLoad() {
  }

  save() {
    this.submitAttempt = true;
    if (this.formValidation.valid) {
      this.salvarFuncionario();
    }
  }

  private salvarFuncionario() {
    //editando
    if (this.funcionario.id) {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.funcionarioProvider.update(this.funcionario)
         .then(data => {
           this.toast.create({ message: 'Funcionario atualizado.', duration: 3000, position: 'botton' }).present();
           this.navCtrl.pop();
           loader.dismiss();
         });

         
       });        
    //criando novo
    } else {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.funcionarioProvider.insert(this.funcionario)
         .then(data => {
           this.toast.create({ message: 'Funcionario salvo.', duration: 3000, position: 'botton' }).present();
         });
         loader.dismiss();
         this.navCtrl.pop();
       });
    }
  }
}
