import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController, NavParams, ToastController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Produto, ProdutosProvider } from '../../providers/produtos/produtos';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the AddProdutoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-produto',
  templateUrl: 'add-produto.html',
})

export class AddProdutoPage {

  produto: Produto;
  formValidation: FormGroup;
  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController, formBuilder: FormBuilder, public navParams: NavParams, public loading: LoadingController, private toast: ToastController, private sqlite: SQLite, private produtoProvider: ProdutosProvider) {
    this.produto = new Produto();
    
      if (this.navParams.data.id) {
        this.produtoProvider.get(this.navParams.data.id)
          .then((result: any) => {
            this.produto = result;
          })
      }

      this.formValidation = formBuilder.group({
        nome: ['', [Validators.maxLength(200), Validators.required]],
        preco_custo: ['', [Validators.maxLength(200), Validators.required]],
        preco_venda: ['', [Validators.maxLength(200), Validators.required]]
      });

  }

  ionViewDidLoad() {
  }

  save() {
    this.submitAttempt = true;
    if (this.formValidation.valid) {
      this.salvarProduto();
    }
  }

  private salvarProduto() {
    //editando
    // if (this.produto.id) {
    //   let loader = this.loading.create({content: 'Carregando...'});
    //    loader.present().then(() => {
    //      this.produtoProvider.update(this.produto)
    //      .then(data => {
    //        this.toast.create({ message: 'Produto atualizado.', duration: 3000, position: 'botton' }).present();
    //      });
    //      loader.dismiss();
    //      this.navCtrl.pop();
    //    });        
    // //criando novo
    // } else {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.produtoProvider.insert(this.produto)
         .then(data => {
           this.toast.create({ message: 'Produto salvo.', duration: 3000, position: 'botton' }).present();
           this.navCtrl.pop();
           loader.dismiss();
         });
         
       });
    }
  // }
}
