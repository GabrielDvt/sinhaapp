import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddLojaPage } from './add-loja';

@NgModule({
  declarations: [
    AddLojaPage,
  ],
  imports: [
    IonicPageModule.forChild(AddLojaPage),
  ],
})
export class AddLojaPageModule {}
