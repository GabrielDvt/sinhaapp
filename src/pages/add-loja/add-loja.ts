import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController, NavParams, ToastController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Loja, LojasProvider } from '../../providers/lojas/lojas';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the AddLojaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-loja',
  templateUrl: 'add-loja.html',
})

export class AddLojaPage {

  loja: Loja;
  formValidation: FormGroup;
  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController, formBuilder: FormBuilder, public navParams: NavParams, public loading: LoadingController, private toast: ToastController, private sqlite: SQLite, private lojaProvider: LojasProvider) {
    this.loja = new Loja();
    
      if (this.navParams.data.id) {
        let loader = this.loading.create({content: "Buscando dados..."});
        loader.present();
        this.lojaProvider.get(this.navParams.data.id)
          .then((result: any) => {
            this.loja = result;
          })
        loader.dismiss();
      }
    
      this.formValidation = formBuilder.group({
        nome: ['', [Validators.maxLength(200), Validators.required]],
        ddd: ['', [Validators.maxLength(3)]],
        telefone: ['', [Validators.maxLength(200), Validators.required]],
        endereco: ['', [Validators.maxLength(200)]]
      });

  }

  ionViewDidLoad() {
  }

  save() {
    this.submitAttempt = true;
    if (this.formValidation.valid) {
      this.salvarLoja();
    }
  }

  private salvarLoja() {
    //editando
    if (this.loja.id) {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.lojaProvider.update(this.loja)
         .then(data => {
           this.toast.create({ message: 'Loja atualizada.', duration: 3000, position: 'botton' }).present();
           this.navCtrl.pop();
           loader.dismiss();
         });
       });        
    //criando novo
    } else {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.lojaProvider.insert(this.loja)
         .then(data => {
           this.toast.create({ message: 'Loja salva.', duration: 3000, position: 'botton' }).present();
         });
         loader.dismiss();
         this.navCtrl.pop();
       });
    }
  }
}
