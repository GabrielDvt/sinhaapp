import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { VendasProvider } from '../../providers/vendas/vendas';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
/**
 * Generated class for the EstatisticasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-estatisticas',
  templateUrl: 'estatisticas.html',
})

export class EstatisticasPage {
   
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('doughnutCanvas') doughnutCanvas;

barChart: any;
doughnutChart: any;
vendasPorProduto: any;
tamanho: number;

nomesProdutos: string[] = Array();
quantidades: number[] = Array();

dtInicial: string;
dtFinal: string;

formValidation: FormGroup;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, formBuilder: FormBuilder, public loading: LoadingController, public vendasProvider: VendasProvider, public navParams: NavParams) {

    this.formValidation = formBuilder.group({
      dtInicial: ['', [Validators.maxLength(200), Validators.required]],
      dtFinal: ['', [Validators.maxLength(200), Validators.required]]
    });

  }

  ionViewDidLoad() {
    let loader = this.loading.create({content: 'Listando os dados...'});
    loader.present().then(() => {
      this.vendasProvider.getQtdePorProduto()
       .then(data => {
         this.vendasPorProduto = data;
         this.tamanho = this.vendasPorProduto.length;
         
         this.createBarChart();

         this.createDoughnutChart();

         loader.dismiss();  
       });
    });
  }

  atualizarGrafico() {
    if (this.formValidation.valid) {
      let loader = this.loading.create({content: "Buscando dados..."});
      loader.present().then(() => {
        this.vendasProvider.getQtdePorProdutoComIntervalo(this.dtInicial, this.dtFinal)
          .then(data => {
            console.log(data);
            this.vendasPorProduto = data;
            this.tamanho =  this.vendasPorProduto.length;
            
            //reseta os gráficos e plota com os novos dados
            this.barChart.destroy();
            this.doughnutChart.destroy();

            //recria os gráficos
            this.createBarChart();
            this.createDoughnutChart();
            loader.dismiss();
          });
      });
    } else {
      let alert = this.alertCtrl.create({
          title: 'Atenção',
          subTitle: 'Preencha ambas as datas!',
          buttons: ['Ok']
        });
        alert.present();
    }
  }


  createBarChart() {
      //cria o gráfico
      this.barChart = new Chart(this.barCanvas.nativeElement, {

          type: 'bar',
          data: {
              labels: [],
              datasets: [{
                  label: 'Produtos vendidos',
                  data: [],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                       'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                       'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                  ],
                  borderColor: [
                      'rgba(255,99,132,1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(255,99,132,1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(255,99,132,1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                  ],
                  borderWidth: 1
              }],
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }

      });

      //insere os dados no chart
      for(var _i = 0 ; _i < this.tamanho; _i++) {
        this.barChart.data.labels.push(this.vendasPorProduto[_i].nome);
        this.barChart.data.datasets.forEach((dataset) => {
            dataset.data.push(parseInt(this.vendasPorProduto[_i].quantidade));
        });
      }
      //faz o update
      this.barChart.update();
  }

  createDoughnutChart() {
      this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
          type: 'doughnut',
          data: {
              labels: [],
              datasets: [{
                  label: '# of Votes',
                  data: [],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  hoverBackgroundColor: [
                      "#FF6384",
                      "#36A2EB",
                      "#FFCE56",
                      "#FF6384",
                      "#36A2EB",
                      "#FFCE56"
                  ]
              }]
          }
      });

      //insere os dados no chart
      for(var _i = 0 ; _i < this.tamanho; _i++) {
        this.doughnutChart.data.labels.push(this.vendasPorProduto[_i].nome);
        this.doughnutChart.data.datasets.forEach((dataset) => {
            dataset.data.push(parseInt(this.vendasPorProduto[_i].quantidade));
        });
      }
      //faz o update
      this.doughnutChart.update();
  }
}
