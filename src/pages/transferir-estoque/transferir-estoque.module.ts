import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransferirEstoquePage } from './transferir-estoque';

@NgModule({
  declarations: [
    TransferirEstoquePage,
  ],
  imports: [
    IonicPageModule.forChild(TransferirEstoquePage),
  ],
})
export class TransferirEstoquePageModule {}
