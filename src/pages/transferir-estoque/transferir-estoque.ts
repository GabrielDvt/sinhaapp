import { Component,  } from '@angular/core';
import { IonicPage, AlertController, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Loja, LojasProvider } from '../../providers/lojas/lojas';
import { Estoque, EstoquesProvider } from '../../providers/estoques/estoques';

/**
 * Generated class for the TransferirEstoquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transferir-estoque',
  templateUrl: 'transferir-estoque.html',
})
export class TransferirEstoquePage {

  lojas: any;
  idEstoque: number;
  estoque: Estoque;
  nomeProduto: string;
  quantidade: number;
  idLoja: number;
  unidade: string;  

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private toast: ToastController, public navParams: NavParams, public loading: LoadingController, public lojasProvider: LojasProvider, public estoquesProvider: EstoquesProvider) {
  	this.idEstoque = this.navParams.get("idEstoque");
    this.nomeProduto = this.navParams.get("nomeProduto");
    this.quantidade = this.navParams.get("quantidade");
    this.unidade = this.navParams.get("unidade");
    this.idLoja = this.navParams.get("idLoja");
    this.estoque = new Estoque();
  }

  getLojas() {
  	this.lojasProvider.getAll()
  	  .then((result: any) => {
  	    //busca todas as lojas
        this.lojas = result;
        //remove a loja atual da lista de lojas (não faz sentido termos a filial aqui)
        this.lojas.splice(this.lojas.findIndex(x => x.id == this.idLoja), 1);      
      })
  }

  getEstoque() {
    this.estoquesProvider.get(this.idEstoque)
      .then((result: any) => {
        this.estoque = result;
      })
  }

  save() {
    //verifica se a quantidade que o usuário colocou é inferior à quantidade que possui em estoque
    if (this.estoque.quantidade <= this.quantidade) {
      console.log("ok");
    } else {
      console.log("não ok");
    }
  }

  ionViewDidLoad() {
    this.getLojas();  
  }

}
