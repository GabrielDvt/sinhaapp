import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Produto, ProdutosProvider } from '../../providers/produtos/produtos';
import { AddProdutoPage } from '../add-produto/add-produto';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the ProdutosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-produtos',
  templateUrl: 'produtos.html',
})
export class ProdutosPage {

  produtos: any;

  constructor(public navCtrl: NavController, public http: HttpClient, public loading: LoadingController, public navParams: NavParams,  private alertCtrl: AlertController, private sqlite: SQLite, private toast: ToastController, private produtoProvider: ProdutosProvider) {
        
  }

  ionViewDidEnter() {
    this.getProdutos();
  }

  getProdutos() {
     let loader = this.loading.create({content: 'Carregando...'});
      loader.present().then(() => {
        this.produtoProvider.getAll()
        .then(data => {
          this.produtos = data;
        });
        loader.dismiss();
      });
  }

  adicionar() {
    this.navCtrl.push('AddProdutoPage');
  }

  editarProduto(id: number) {
    this.navCtrl.push('AddProdutoPage', {id: id});
  }

  removerProduto(produto: Produto) {
     let alert = this.alertCtrl.create({
       title: 'Confirmar ação',
       message: 'Deseja realmente excluir este produto?',
       buttons: [
         {
           text: 'Cancelar',
           role: 'cancelar',
           handler: () => {
           }
         },
         {
           text: 'Confirmar',
           handler: () => {
             this.produtoProvider.remove(produto.id)
               .then(() => {
                 // Removendo do array de produtos
                 var index = this.produtos.indexOf(produto);
                 this.produtos.splice(index, 1);
                 this.toast.create({ message: 'Produto removido.', duration: 3000, position: 'botton' }).present();
               });
           }
         }
       ]
     });
     alert.present();
  }
   
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProdutosPage');
  }

}
