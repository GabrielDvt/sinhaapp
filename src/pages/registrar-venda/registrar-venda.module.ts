import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrarVendasPage } from './registrar-venda';

@NgModule({
  declarations: [
    RegistrarVendasPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrarVendasPage),
  ],
})
export class RegistrarVendaPageModule {}
