import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Produto, ProdutosProvider } from '../../providers/produtos/produtos';
import { AddProdutoPage } from '../add-produto/add-produto';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the ProdutosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrar-venda',
  templateUrl: 'registrar-venda.html',
})

export class RegistrarVendasPage {

  produtos: any;

  constructor(public navCtrl: NavController, public http: HttpClient, public loading: LoadingController, public navParams: NavParams,  private alertCtrl: AlertController, private sqlite: SQLite, private toast: ToastController, private produtoProvider: ProdutosProvider) { 
  }

  ionViewDidEnter() {
    this.getProdutos();
  }

  getProdutos() {
     let loader = this.loading.create({content: 'Buscando produtos...'});
       loader.present();

      this.produtoProvider.getAll()
      .then(data => {
        this.produtos = data;
      });

      loader.dismiss();  
  }

  registrarVenda(produto: Produto) {
    this.navCtrl.push("AddVendaPage", {produto: produto});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProdutosPage');
  }

}
