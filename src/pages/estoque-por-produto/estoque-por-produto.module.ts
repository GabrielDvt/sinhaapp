import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstoquePorProdutoPage } from './estoque-por-produto';

@NgModule({
  declarations: [
    EstoquePorProdutoPage,
  ],
  imports: [
    IonicPageModule.forChild(EstoquePorProdutoPage),
  ],
})
export class EstoquePorProdutoPageModule {}
