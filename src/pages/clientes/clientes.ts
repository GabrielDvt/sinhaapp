import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Cliente, ClientesProvider } from '../../providers/clientes/clientes';
import { AddClientePage } from '../add-cliente/add-cliente';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the ClientesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-clientes',
  templateUrl: 'clientes.html',
})
export class ClientesPage {

  clientes: any;

  constructor(public navCtrl: NavController, public http: HttpClient, public loading: LoadingController, public navParams: NavParams,  private alertCtrl: AlertController, private sqlite: SQLite, private toast: ToastController, private clienteProvider: ClientesProvider) {
		  	
  }

  ionViewDidEnter() {
    this.getClientes();
  }

  getClientes() {
     let loader = this.loading.create({content: 'Buscando clientes...'});
      loader.present();
      this.clienteProvider.getAll()
        .then(data => {
          this.clientes = data;
        });
      loader.dismiss();
  }

  adicionar() {
    this.navCtrl.push("AddClientePage");
  }

  editarCliente(id: number) {
    this.navCtrl.push("AddClientePage", {id: id});
  }

  removerCliente(cliente: Cliente) {
     let alert = this.alertCtrl.create({
       title: 'Confirmar ação',
       message: 'Deseja realmente excluir este cliente?',
       buttons: [
         {
           text: 'Cancelar',
           role: 'cancelar',
           handler: () => {
           }
         },
         {
           text: 'Confirmar',
           handler: () => {
             this.clienteProvider.remove(cliente.id)
               .then(() => {
                 // Removendo do array de produtos
                 var index = this.clientes.indexOf(cliente);
                 this.clientes.splice(index, 1);
                 this.toast.create({ message: 'Cliente removido.', duration: 3000, position: 'botton' }).present();
               });
           }
         }
       ]
     });
     alert.present();
  }
   

}
