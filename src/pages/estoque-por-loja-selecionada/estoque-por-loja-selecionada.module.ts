import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstoquePorLojaSelecionadaPage } from './estoque-por-loja-selecionada';

@NgModule({
  declarations: [
    EstoquePorLojaSelecionadaPage,
  ],
  imports: [
    IonicPageModule.forChild(EstoquePorLojaSelecionadaPage),
  ],
})
export class EstoquePorLojaSelecionadaPageModule {}
