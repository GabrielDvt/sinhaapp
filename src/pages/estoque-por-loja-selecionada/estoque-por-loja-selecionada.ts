import { Component,  } from '@angular/core';
import { IonicPage, AlertController, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Loja, LojasProvider } from '../../providers/lojas/lojas';
import { Estoque, EstoquesProvider } from '../../providers/estoques/estoques';

/**
 * Generated class for the EstoquePorLojaSelecionadaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-estoque-por-loja-selecionada',
  templateUrl: 'estoque-por-loja-selecionada.html',
})
export class EstoquePorLojaSelecionadaPage {

  loja: Loja;
  estoques: any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private toast: ToastController, public navParams: NavParams, public loading: LoadingController, public estoquesProvider: EstoquesProvider) {
  	this.loja = this.navParams.get("loja");
  }

  ionViewDidEnter() {
    let loader = this.loading.create({content: 'Buscando estoquqe...'});
    loader.present().then(() => {
      this.getEstoquesPorLoja();
    });
    loader.dismiss();
  }

  getEstoquesPorLoja() {
    this.estoquesProvider.getEstoquePorLoja(this.loja.id)
      .then(data => {
        this.estoques = data;
      });  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EstoquePorLojaSelecionadaPage');
  }

  adicionarEstoque() {
  	this.navCtrl.push("AddEstoquePage", {loja: this.loja});
  }

  transferir(idEstoque, nomeProduto, quantidade, unidade) {
    this.navCtrl.push("TransferirEstoquePage", {idEstoque: idEstoque, nomeProduto: nomeProduto, quantidade: quantidade, idLoja: this.loja.id, unidade: unidade});
  }

  excluir(idEstoque) {
    let alert = this.alertCtrl.create({
      title: 'Confirmar ação',
      message: 'Deseja realmente excluir este item do estoque?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
          handler: () => {
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.estoquesProvider.remove(idEstoque)
              .then(() => {
                // Removendo do array de estoques
                var index = this.estoques.indexOf(idEstoque);
                this.estoques.splice(index, 1);
                this.toast.create({ message: 'Estoque removido.', duration: 3000, position: 'botton' }).present();
              });
          }
        }
      ]
    });
    alert.present();
  }

}
