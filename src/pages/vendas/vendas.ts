import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Venda, VendasProvider } from '../../providers/vendas/vendas';
import { AddVendaPage } from '../add-venda/add-venda';
import { RegistrarVendasPage } from '../registrar-venda/registrar-venda';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the VendasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vendas',
  templateUrl: 'vendas.html',
})
export class VendasPage {

  vendas: any;

  constructor(public navCtrl: NavController, public http: HttpClient, public loading: LoadingController, public navParams: NavParams,  private alertCtrl: AlertController, private sqlite: SQLite, private toast: ToastController, private vendaProvider: VendasProvider) {
        
  }

  ionViewDidEnter() {
    this.getVendas();
  }

  getVendas() {
     let loader = this.loading.create({content: 'Listando as vendas...'});
     loader.present().then(() => {
       this.vendaProvider.getAll()
        .then(data => {
          this.vendas = data;
          loader.dismiss();  
        });
     });    
  }

  adicionar() {
    this.navCtrl.push("RegistrarVendasPage");
  }

  editarVenda(id: number) {
    this.navCtrl.push('AddVendaPage', {id: id});
  }

  removerVenda(venda: Venda) {
     let alert = this.alertCtrl.create({
       title: 'Confirmar ação',
       message: 'Deseja realmente excluir este venda?',
       buttons: [
         {
           text: 'Cancelar',
           role: 'cancelar',
           handler: () => {
           }
         },
         {
           text: 'Confirmar',
           handler: () => {
             let loader = this.loading.create({content: "Excluindo venda..."});
             loader.present().then(() => {
               this.vendaProvider.remove(venda.id)
                 .then(() => {
                   // Removendo do array de vendas
                   var index = this.vendas.indexOf(venda);
                   this.vendas.splice(index, 1);
                   this.toast.create({ message: 'Venda removida.', duration: 3000, position: 'botton' }).present();
                   loader.dismiss();
                 });
             });
             
           }
         }
       ]
     });
     alert.present();
  }
   
  ionViewDidLoad() {
    console.log('ionViewDidLoad VendasPage');
  }

}
