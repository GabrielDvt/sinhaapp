import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController, NavParams, ToastController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Cliente, ClientesProvider } from '../../providers/clientes/clientes';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the AddClientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-cliente',
  templateUrl: 'add-cliente.html',
})

export class AddClientePage {

  cliente: Cliente;
  formValidation: FormGroup;
  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController, formBuilder: FormBuilder, public navParams: NavParams, public loading: LoadingController, private toast: ToastController, private sqlite: SQLite, private clienteProvider: ClientesProvider) {
    this.cliente = new Cliente();
    
      if (this.navParams.data.id) {
        let loader = this.loading.create({content: "Buscando dados..."});
        loader.present();
        this.clienteProvider.get(this.navParams.data.id)
          .then((result: any) => {
            this.cliente = result;
          })
        loader.dismiss();
      }
    
      this.formValidation = formBuilder.group({
        nome: ['', [Validators.maxLength(200), Validators.required]],
        telefone: ['', [Validators.maxLength(200), Validators.required]],
        email: ['', [Validators.maxLength(200)]]
      });

  }

  ionViewDidLoad() {
  }

  save() {
    this.submitAttempt = true;
    if (this.formValidation.valid) {
      this.salvarCliente();
    }
  }

  private salvarCliente() {
    //editando
    if (this.cliente.id) {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.clienteProvider.update(this.cliente)
         .then(data => {
           this.toast.create({ message: 'Cliente atualizado.', duration: 3000, position: 'botton' }).present();
           this.navCtrl.pop();
           loader.dismiss();
         });
         
         
       });        
    //criando novo
    } else {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.clienteProvider.insert(this.cliente)
         .then(data => {
           this.toast.create({ message: 'Cliente salvo.', duration: 3000, position: 'botton' }).present();
         });
         loader.dismiss();
         this.navCtrl.pop();
       });
    }
  }
}
