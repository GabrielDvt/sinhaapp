import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddVendaPage } from './add-venda';

@NgModule({
  declarations: [
    AddVendaPage,
  ],
  imports: [
    IonicPageModule.forChild(AddVendaPage),
  ],
})
export class AddVendaPageModule {}
