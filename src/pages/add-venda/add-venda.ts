import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController, NavParams, ToastController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Venda, VendasProvider } from '../../providers/vendas/vendas';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Produto } from '../../providers/produtos/produtos';
import { Cliente, ClientesProvider } from '../../providers/clientes/clientes';

/**
 * Generated class for the AddVendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-venda',
  templateUrl: 'add-venda.html',
})

export class AddVendaPage {

  venda: Venda;
  submitAttempt: boolean = false;
  produto: Produto;
  cliente: Cliente;
  clientes: any;
  nomeProduto: string;
  formValidation: FormGroup;

  constructor(public navCtrl: NavController, private clienteProvider: ClientesProvider, formBuilder: FormBuilder, public navParams: NavParams, public loading: LoadingController, private toast: ToastController, private sqlite: SQLite, private vendaProvider: VendasProvider) {
    
    this.formValidation = formBuilder.group({
      produto_id: ['', [Validators.required]],
      cliente_id: ['', [Validators.required]],
      pagamento: ['', [Validators.required]],
      data: ['', [Validators.required]],
      nomeProduto: ['', [Validators.maxLength(200), Validators.required]],
      preco_unidade: ['', [Validators.maxLength(10), Validators.required, Validators.pattern("[+-]?([0-9]*[.])?[0-9]+")]],
      quantidade: ['', [Validators.required, Validators.pattern("[0-9]*")]],
      total: ['', [Validators.maxLength(10), Validators.required, Validators.pattern("[+-]?([0-9]*[.])?[0-9]+")]],
    });

    this.venda = new Venda();
    
    this.carregarClientes();
    
    if (this.navParams.data.id) {
      this.carregarVendas();
    } else {
      this.venda.pagamento = "Dinheiro";
    }

    if (this.navParams.data.produto) {
      this.nomeProduto = this.navParams.data.produto.nome;
      this.venda.produto_id = this.navParams.data.produto.id;
      this.venda.preco_unidade = this.navParams.data.produto.preco_venda;
    }

  }

  ionViewDidLoad() {
  }

  carregarVendas() {
    let loader = this.loading.create({content: 'Carregando vendas...'});
    loader.present().then(() => {
      this.vendaProvider.get(this.navParams.data.id)
       .then((result: any) => {
          this.venda = result;
          loader.dismiss();
       })
    });
  }

  carregarClientes() {
    let loader = this.loading.create({content: 'Carregando clientes...'});
    loader.present();
      
      this.clienteProvider.getAll()
        .then((result: any) => {
           this.clientes = result;
        })
    loader.dismiss();
  }

  save() {
    this.submitAttempt = true;
    if (this.formValidation.valid) {
      this.salvarVenda();
    }
  }

  calculaTotal() {
    this.venda.total = this.venda.quantidade * this.venda.preco_unidade;
  }

  private salvarVenda() {
    //editando
    if (this.venda.id) {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.vendaProvider.update(this.venda)
         .then(data => {
           console.log(this.venda);
           this.toast.create({ message: 'Venda atualizada.', duration: 3000, position: 'botton' }).present();
         });
         loader.dismiss();
         this.navCtrl.pop();
       });        
    //criando novo
    } else {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.vendaProvider.insert(this.venda)
         .then(data => {

           this.toast.create({ message: 'Venda salva.', duration: 3000, position: 'botton' }).present();
         });
         loader.dismiss();
         this.navCtrl.pop();
       });
    }
  }
}
