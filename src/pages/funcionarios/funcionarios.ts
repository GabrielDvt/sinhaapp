import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Funcionario, FuncionariosProvider } from '../../providers/funcionarios/funcionarios';
import { AddFuncionarioPage } from '../add-funcionario/add-funcionario';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the FuncionariosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-funcionarios',
  templateUrl: 'funcionarios.html',
})
export class FuncionariosPage {

  funcionarios: any;

  constructor(public navCtrl: NavController, public http: HttpClient, public loading: LoadingController, public navParams: NavParams,  private alertCtrl: AlertController, private sqlite: SQLite, private toast: ToastController, private funcionarioProvider: FuncionariosProvider) {
        
  }

  ionViewDidEnter() {
    let loader = this.loading.create({content: 'Buscando funcionários...'});
    loader.present().then(() => {
      this.getFuncionarios();
    });
    loader.dismiss();
  }


  getFuncionarios() {
    this.funcionarioProvider.getAll()
      .then(data => {
        this.funcionarios = data;
      });  
  }

  adicionar() {
    this.navCtrl.push('AddFuncionarioPage');
  }

  editarFuncionario(id: number) {
    this.navCtrl.push('AddFuncionarioPage', {id: id});
  }

  removerFuncionario(funcionario: Funcionario) {
     let alert = this.alertCtrl.create({
       title: 'Confirmar ação',
       message: 'Deseja realmente excluir este funcionario?',
       buttons: [
         {
           text: 'Cancelar',
           role: 'cancelar',
           handler: () => {
           }
         },
         {
           text: 'Confirmar',
           handler: () => {
             this.funcionarioProvider.remove(funcionario.id)
               .then(() => {
                 // Removendo do array de produtos
                 var index = this.funcionarios.indexOf(funcionario);
                 this.funcionarios.splice(index, 1);
                 this.toast.create({ message: 'Funcionario removido.', duration: 3000, position: 'botton' }).present();
               });
           }
         }
       ]
     });
     alert.present();
  }
   
}
