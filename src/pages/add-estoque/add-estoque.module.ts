import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEstoquePage } from './add-estoque';

@NgModule({
  declarations: [
    AddEstoquePage,
  ],
  imports: [
    IonicPageModule.forChild(AddEstoquePage),
  ],
})
export class AddEstoquePageModule {}
