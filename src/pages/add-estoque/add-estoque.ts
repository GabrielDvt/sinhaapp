import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { Loja, LojasProvider } from '../../providers/lojas/lojas';
import { Produto, ProdutosProvider } from '../../providers/produtos/produtos';
import { UnidadesProvider, Unidade } from '../../providers/unidades/unidades';
import { EstoquesProvider, Estoque } from '../../providers/estoques/estoques';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
/**
 * Generated class for the AddEstoquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-estoque',
  templateUrl: 'add-estoque.html',
})

export class AddEstoquePage {

  loja: Loja;
  estoque: Estoque;
  unidades: any;
  produtos: any;
  formValidation: FormGroup;
  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController,  formBuilder: FormBuilder,  public estoquesProvider: EstoquesProvider, public unidadesProvider: UnidadesProvider, public navParams: NavParams, public produtosProvider: ProdutosProvider, public loading: LoadingController, private toast: ToastController) {
  	this.formValidation = formBuilder.group({
      produto_id: ['', [Validators.required]],
      quantidade: ['', [Validators.required]],
      unidade_id: ['', [Validators.required]],
      loja_id: ['', [Validators.required]]
    });

    this.estoque = new Estoque();
    this.loja = this.navParams.get("loja");
  	this.getProdutos();
  	this.getUnidades();
  }

  getUnidades() {
     let loader = this.loading.create({content: 'Carregando unidades...'});
      loader.present().then(() => {
        this.unidadesProvider.getAll()
        .then(data => {
          this.unidades = data;
        });
        loader.dismiss();
      });
  }

  getProdutos() {
     let loader = this.loading.create({content: 'Carregando...'});
      loader.present().then(() => {
        this.produtosProvider.getAll()
        .then(data => {
          this.produtos = data;
        });
        loader.dismiss();
      });
  }

  save() {
    this.estoque.quantidade = +(this.estoque.quantidade);
    this.estoque.unidade_id = +(this.estoque.unidade_id);
    this.salvarEstoque();
  }

  salvarEstoque() {
    //editando
    if (this.estoque.id) {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.estoquesProvider.update(this.estoque)
         .then(data => {
           this.toast.create({ message: 'Estoque atualizado.', duration: 3000, position: 'botton' }).present();
           this.navCtrl.pop();
           loader.dismiss();
         });
       });        
    //criando novo
    } else {
      let loader = this.loading.create({content: 'Carregando...'});
       loader.present().then(() => {
         this.estoquesProvider.insert(this.estoque)
         .then(data => {
           this.toast.create({ message: 'Estoque salvo.', duration: 3000, position: 'botton' }).present();
         });
         loader.dismiss();
         this.navCtrl.pop();
       });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEstoquePage');
  }

}
