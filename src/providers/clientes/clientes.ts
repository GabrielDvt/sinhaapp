import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
/*
  Generated class for the ClientesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ClientesProvider {

  private API_URL = 'http://gabrieldevito.com/public/api/clientes';

  constructor(public http: HttpClient, private dbProvider: DatabaseProvider) {
  	
  }

  insert(cliente: Cliente) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
        this.http.post(this.API_URL, JSON.stringify(cliente), httpOptions)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(JSON.stringify(err));
          });
      });
  }

  getAll() {
    return new Promise(resolve => {
      this.http.get(this.API_URL)
        .subscribe(data => {
        resolve(data);
      }, err => {
        console.error(JSON.stringify(err));
      });
    });
  }

  get(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
     
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  update(cliente: Cliente) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + cliente.id;
      let data = JSON.stringify(cliente);
       
      this.http.put(url, data, httpOptions)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  remove(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
 
      this.http.delete(url)
        .subscribe((result: any) => {
          resolve(JSON.stringify(result));
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }


  /*public insert(cliente: Cliente) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'insert into clientes (nome, telefone, email) values (?,?,?)';
          let data = [cliente.nome, cliente.telefone, cliente.email];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public update(cliente: Cliente) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'update clientes set nome = ?, telefone = ?, email = ? where id = ?';
          let data = [cliente.nome, cliente.telefone, cliente.email, cliente.id];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public get(id: number) {
        return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
            let sql = 'select * from clientes where id = ?';
            let data = [id];
     
            return db.executeSql(sql, data)
              .then((data: any) => {
                if (data.rows.length > 0) {
                  let item = data.rows.item(0);
                  let cliente = new Cliente();
                  cliente.id = item.id;
                  cliente.nome = item.nome;
                  cliente.email = item.email;
                  cliente.telefone = item.telefone;
     
                  return cliente;
                }
     
                return null;
              })
              .catch((e) => console.error(e));
          })
          .catch((e) => console.error(e));
      }

    public remove(id: number) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'delete from clientes where id = ?';
          let data = [id];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public getAll() {
       return this.dbProvider.getDB()
         .then((db: SQLiteObject) => {
           let sql = 'SELECT * FROM clientes';
          
           return db.executeSql(sql, "")
             .then((data: any) => {
               if (data.rows.length > 0) {
                 let clientes: any[] = [];
                 for (var i = 0; i < data.rows.length; i++) {
                   var cliente = data.rows.item(i);
                   clientes.push(cliente);
                 }
                 return clientes;
               } else {
                 return [];
               }
             })
             .catch((e) => console.error(e));
         })
         .catch((e) => console.error(e));
     }

}
*/
}

export class Cliente {
  id: number;
  nome: string;
  telefone: string;
  email: string;
}
