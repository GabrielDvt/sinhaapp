import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
/*
  Generated class for the VendasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VendasProvider {

  private API_URL = 'http://gabrieldevito.com/public/api/vendas';
  private URL_QTDE_POR_PRODUTO = "http://gabrieldevito.com/public/api/buscarQtdePorProduto";


  constructor(public http: HttpClient, private dbProvider: DatabaseProvider) {
    
  }

  insert(venda: Venda) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
        this.http.post(this.API_URL, JSON.stringify(venda), httpOptions)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(JSON.stringify(err));
          });
      });
  }

  getQtdePorProduto() {
    return new Promise(resolve => {
      this.http.get(this.URL_QTDE_POR_PRODUTO)
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  /* mesmo que a função acima, mas adicionado um intervalo de data */
  getQtdePorProdutoComIntervalo(dtInicial, dtFinal) {
    return new Promise(resolve => {
      this.http.get(this.URL_QTDE_POR_PRODUTO + "/" + dtInicial + '/' + dtFinal)
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  getAll() {
    return new Promise(resolve => {
      this.http.get(this.API_URL)
        .subscribe(data => {
        resolve(data);
      }, err => {
        console.error(JSON.stringify(err));
      });
    });
  }

  get(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  update(venda: Venda) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + venda.id;
      let data = JSON.stringify(venda);
       
      this.http.put(url, data, httpOptions)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  remove(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
 
      this.http.delete(url)
        .subscribe((result: any) => {
          resolve(JSON.stringify(result));
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }


}

export class Venda {
  id: number;
  nome: string;
  venda_id: number;
  funcionario_id: number;
  produto_id: number;
  quantidade: number;
  preco_unidade: number;
  desconto: number;
  pagamento: string;
  total: number;
  data: Date;
}
