import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
/*
  Generated class for the ProdutosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProdutosProvider {

  private API_URL = 'http://gabrieldevito.com/public/api/produtos';

  constructor(public http: HttpClient, private dbProvider: DatabaseProvider, private sqlite: SQLite) {
    
  }

  // insert(produto: Produto) {

  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Content-Type':  'application/json',
  //       'Authorization': 'my-auth-token'
  //     })
  //   };

  //   return new Promise((resolve, reject) => {
  //       this.http.post(this.API_URL, JSON.stringify(produto), httpOptions)
  //         .subscribe(res => {
  //           resolve(res);
  //         }, (err) => {
  //           reject(JSON.stringify(err));
  //         });
  //     });
  // }

  // getAll() {
  //   return new Promise(resolve => {
  //     this.http.get(this.API_URL)
  //       .subscribe(data => {
  //       resolve(data);
  //     }, err => {
  //       console.error(JSON.stringify(err));
  //     });
  //   });
  // }

  // get(id: number) {
  //   return new Promise((resolve, reject) => {
  //     let url = this.API_URL + '/' + id;
     
  //     this.http.get(url)
  //       .subscribe((result: any) => {
  //         resolve(result);
  //       },
  //       (error) => {
  //         reject(JSON.stringify(error));
  //       });
  //   });
  // }

  // update(produto: Produto) {
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Content-Type':  'application/json',
  //       'Authorization': 'my-auth-token'
  //     })
  //   };

  //   return new Promise((resolve, reject) => {
  //     let url = this.API_URL + '/' + produto.id;
  //     let data = JSON.stringify(produto);
       
  //     this.http.put(url, data, httpOptions)
  //       .subscribe((result: any) => {
  //         resolve(result);
  //       },
  //       (error) => {
  //         reject(JSON.stringify(error));
  //       });
  //   });
  // }

  // remove(id: number) {
  //   return new Promise((resolve, reject) => {
  //     let url = this.API_URL + '/' + id;
 
  //     this.http.delete(url)
  //       .subscribe((result: any) => {
  //         resolve(JSON.stringify(result));
  //       },
  //       (error) => {
  //         reject(JSON.stringify(error));
  //       });
  //   });
  // }


  public insert(produto: Produto) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'insert into produtos (nome, preco_custo, preco_venda) values (?,?,?)';
          let data = [produto.nome, produto.preco_custo, produto.preco_venda];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    // public update(produto: Produto) {
    //   return this.dbProvider.getDB()
    //     .then((db: SQLiteObject) => {
    //       let sql = 'update produtos set nome = ?, telefone = ?, email = ? where id = ?';
    //       let data = [produto.nome, produto.telefone, produto.email, produto.id];

    //       return db.executeSql(sql, data)
    //         .catch((e) => console.error(e));
    //     })
    //     .catch((e) => console.error(e));
    // }

    public get(id: number) {
        return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
            let sql = 'select * from produtos where id = ?';
            let data = [id];
     
            return db.executeSql(sql, data)
              .then((data: any) => {
                if (data.rows.length > 0) {
                  let item = data.rows.item(0);
                  let produto = new Produto();
                  produto.id = item.id;
                  produto.nome = item.nome;
                  produto.preco_custo = item.preco_custo;
                  produto.preco_venda = item.preco_venda;
     
                  return produto;
                }
     
                return null;
              })
              .catch((e) => console.error(e));
          })
          .catch((e) => console.error(e));
      }

    public remove(id: number) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'delete from produtos where id = ?';
          let data = [id];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public getAll() {
       return this.dbProvider.getDB()
         .then((db: SQLiteObject) => {
           let sql = 'SELECT * FROM produtos';
           return db.executeSql(sql)
             .then((data: any) => {
               if (data.rows.length > 0) {
                 let produtos: any[] = [];
                 for (var i = 0; i < data.rows.length; i++) {
                   var produto = data.rows.item(i);
                   produtos.push(produto);
                 }
                 return produtos;
               } else {
                 return [];
               }
             })
             .catch((e) => console.error(e));
         })
         .catch((e) => console.error(e));
     }
}


export class Produto {
  id: number;
  nome: String;
  preco_custo: number;
  preco_venda: number;
}