import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  constructor(public http: HttpClient, private sqlite: SQLite) {
  	
  }

  /**
   * Cria um banco caso não exista ou pega um banco existente com o nome no parametro
   */
	public getDB() {
		return this.sqlite.create({
		  name: 'sinha.db',
		  location: 'default'
		});
	}

		/**
	 * Cria a estrutura inicial do banco de dados
	 */
	public createDatabase() {
	  return this.getDB()
	    .then((db: SQLiteObject) => {

	      // Criando as tabelas
	      this.createTables(db);

	      // Inserindo dados padrão
	      //this.insertDefaultItems(db);

	    })
	    .catch(e => console.log(e));
	}

	/**
	* Criando as tabelas no banco de dados
	* @param db
	*/
	private createTables(db: SQLiteObject) {
	// Criando as tabelas
	db.sqlBatch([
	  ["CREATE TABLE IF NOT EXISTS clientes (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT NOT NULL, telefone TEXT, email TEXT)"],
	  ['CREATE TABLE IF NOT EXISTS funcionarios (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT)'],
	  ["CREATE TABLE IF NOT EXISTS produtos (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, preco_custo REAL, preco_venda REAL NOT NULL)"],
	  ["INSERT INTO produtos (nome, preco_custo, preco_venda) VALUES ('teste', 1.5, 1.5)"],
	  ["CREATE TABLE IF NOT EXISTS vendas(id integer primary key AUTOINCREMENT NOT NULL, funcionario_id integer, pagamento TEXT, cliente_id integer NOT NULL, produto_id integer NOT NULL, quantidade integer NOT NULL, preco_unidade REAL NOT NULL, total REAL, data TEXT, FOREIGN KEY(funcionario_id) REFERENCES funcionarios(id), FOREIGN KEY(produto_id) REFERENCES produtos(id), FOREIGN KEY('cliente_id') REFERENCES clientes(id))"]
	])
	  .then(() => console.log('Tabelas criadas'))
	  .catch(e => console.error('Erro ao criar as tabelas', e.toString()));
	}

	 /**
   * Incluindo os dados padrões
   * @param db
   */
  /*private insertDefaultItems(db: SQLiteObject) {
    db.executeSql('select COUNT(id) as qtd from categories', {})
    .then((data: any) => {
      //Se não existe nenhum registro
      if (data.rows.item(0).qtd == 0) {

        // Criando as tabelas
        db.sqlBatch([
          ['insert into categories (name) values (?)', ['Hambúrgueres']],
          ['insert into categories (name) values (?)', ['Bebidas']],
          ['insert into categories (name) values (?)', ['Sobremesas']]
        ])
          .then(() => console.log('Dados padrões incluídos'))
          .catch(e => console.error('Erro ao incluir dados padrões', e));

      }
    })
    .catch(e => console.error('Erro ao consultar a qtd de categorias', e));
  }*/



}
