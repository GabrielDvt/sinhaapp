import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
/*
  Generated class for the LojasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LojasProvider {

  private API_URL = 'http://gabrieldevito.com/public/api/lojas';

  constructor(public http: HttpClient, private dbProvider: DatabaseProvider) {
  	
  }

  insert(loja: Loja) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
        this.http.post(this.API_URL, JSON.stringify(loja), httpOptions)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(JSON.stringify(err));
          });
      });
  }

  getAll() {
    return new Promise(resolve => {
      this.http.get(this.API_URL)
        .subscribe(data => {
        resolve(data);
      }, err => {
        console.error(JSON.stringify(err));
      });
    });
  }

  get(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
     
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  update(loja: Loja) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + loja.id;
      let data = JSON.stringify(loja);
       
      this.http.put(url, data, httpOptions)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  remove(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
 
      this.http.delete(url)
        .subscribe((result: any) => {
          resolve(JSON.stringify(result));
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

}

export class Loja {
  id: number;
  nome: string;
  telefone: string;
  ddd: string;
  endereco: string;
}
