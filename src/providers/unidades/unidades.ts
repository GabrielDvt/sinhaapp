import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
/*
  Generated class for the UnidadesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UnidadesProvider {

  private API_URL = 'http://gabrieldevito.com/public/api/unidades';

  constructor(public http: HttpClient, private dbProvider: DatabaseProvider) {
    
  }

  insert(unidade: Unidade) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
        this.http.post(this.API_URL, JSON.stringify(unidade), httpOptions)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(JSON.stringify(err));
          });
      });
  }

  getAll() {
    return new Promise(resolve => {
      this.http.get(this.API_URL)
        .subscribe(data => {
        resolve(data);
      }, err => {
        console.error(JSON.stringify(err));
      });
    });
  }

  get(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
     
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  update(unidade: Unidade) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + unidade.id;
      let data = JSON.stringify(unidade);
       
      this.http.put(url, data, httpOptions)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  remove(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
 
      this.http.delete(url)
        .subscribe((result: any) => {
          resolve(JSON.stringify(result));
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }


  /*public insert(unidade: Unidade) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'insert into unidades (nome, telefone, email) values (?,?,?)';
          let data = [unidade.nome, unidade.telefone, unidade.email];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public update(unidade: Unidade) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'update unidades set nome = ?, telefone = ?, email = ? where id = ?';
          let data = [unidade.nome, unidade.telefone, unidade.email, unidade.id];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public get(id: number) {
        return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
            let sql = 'select * from unidades where id = ?';
            let data = [id];
     
            return db.executeSql(sql, data)
              .then((data: any) => {
                if (data.rows.length > 0) {
                  let item = data.rows.item(0);
                  let unidade = new Unidade();
                  unidade.id = item.id;
                  unidade.nome = item.nome;
                  unidade.email = item.email;
                  unidade.telefone = item.telefone;
     
                  return unidade;
                }
     
                return null;
              })
              .catch((e) => console.error(e));
          })
          .catch((e) => console.error(e));
      }

    public remove(id: number) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'delete from unidades where id = ?';
          let data = [id];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public getAll() {
       return this.dbProvider.getDB()
         .then((db: SQLiteObject) => {
           let sql = 'SELECT * FROM unidades';
          
           return db.executeSql(sql, "")
             .then((data: any) => {
               if (data.rows.length > 0) {
                 let unidades: any[] = [];
                 for (var i = 0; i < data.rows.length; i++) {
                   var unidade = data.rows.item(i);
                   unidades.push(unidade);
                 }
                 return unidades;
               } else {
                 return [];
               }
             })
             .catch((e) => console.error(e));
         })
         .catch((e) => console.error(e));
     }

}
*/
}

export class Unidade {
  id: number;
  unidade: String;
}