import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
/*
  Generated class for the FuncionariosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FuncionariosProvider {

  private API_URL = 'http://gabrieldevito.com/public/api/funcionarios';

  constructor(public http: HttpClient, private dbProvider: DatabaseProvider) {
    
  }

  insert(funcionario: Funcionario) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
        this.http.post(this.API_URL, JSON.stringify(funcionario), httpOptions)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(JSON.stringify(err));
          });
      });
  }

  getAll() {
    return new Promise(resolve => {
      this.http.get(this.API_URL)
        .subscribe(data => {
        resolve(data);
      }, err => {
        console.error(JSON.stringify(err));
      });
    });
  }

  get(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
     
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  update(funcionario: Funcionario) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + funcionario.id;
      let data = JSON.stringify(funcionario);
       
      this.http.put(url, data, httpOptions)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }

  remove(id: number) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL + '/' + id;
 
      this.http.delete(url)
        .subscribe((result: any) => {
          resolve(JSON.stringify(result));
        },
        (error) => {
          reject(JSON.stringify(error));
        });
    });
  }


  /*public insert(funcionario: Funcionario) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'insert into funcionarios (nome, telefone, email) values (?,?,?)';
          let data = [funcionario.nome, funcionario.telefone, funcionario.email];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public update(funcionario: Funcionario) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'update funcionarios set nome = ?, telefone = ?, email = ? where id = ?';
          let data = [funcionario.nome, funcionario.telefone, funcionario.email, funcionario.id];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public get(id: number) {
        return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
            let sql = 'select * from funcionarios where id = ?';
            let data = [id];
     
            return db.executeSql(sql, data)
              .then((data: any) => {
                if (data.rows.length > 0) {
                  let item = data.rows.item(0);
                  let funcionario = new Funcionario();
                  funcionario.id = item.id;
                  funcionario.nome = item.nome;
                  funcionario.email = item.email;
                  funcionario.telefone = item.telefone;
     
                  return funcionario;
                }
     
                return null;
              })
              .catch((e) => console.error(e));
          })
          .catch((e) => console.error(e));
      }

    public remove(id: number) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'delete from funcionarios where id = ?';
          let data = [id];

          return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public getAll() {
       return this.dbProvider.getDB()
         .then((db: SQLiteObject) => {
           let sql = 'SELECT * FROM funcionarios';
          
           return db.executeSql(sql, "")
             .then((data: any) => {
               if (data.rows.length > 0) {
                 let funcionarios: any[] = [];
                 for (var i = 0; i < data.rows.length; i++) {
                   var funcionario = data.rows.item(i);
                   funcionarios.push(funcionario);
                 }
                 return funcionarios;
               } else {
                 return [];
               }
             })
             .catch((e) => console.error(e));
         })
         .catch((e) => console.error(e));
     }

}
*/
}

export class Funcionario {
  id: number;
  nome: string;
}
